from game.game import Game
from game.view.game_view import GameView
from game.view.game_view_handler import GameViewHandler

if __name__ == '__main__':
    game = Game()
    view = GameView(game)

    GameViewHandler(game, view, against_mcts=True).start_game()
