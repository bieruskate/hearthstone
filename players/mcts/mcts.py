from copy import deepcopy
from math import sqrt, log
from time import time

from anytree import find, RenderTree, findall

from game.combinations_builder import CombinationBuilder
from game.game import Winner
from logger.logger import print_with_color, Color
from players.greedy.greedy_player import FastRandomGreedyPlayer
from players.greedy.state_evaluation_mixin import StateEvaluationMixin
from players.mcts.nodes import StateNode, CardChoiceNode


class MCTS(StateEvaluationMixin):
    def __init__(self, game):
        self.game = game
        self.root = self._add_node(deepcopy(game))

    def perform_moves(self):
        combination = self._find_best_moves_combination(perform_time=5)

        self.print_tree()

        if len(combination) == 0:
            print_with_color('Brak możliwych ruchów do wykonania', Color.WARNING)

        for move in combination:
            print_with_color(move, Color.BLUE)
            self.game.handle_move(move)

        self.game.next_tour()

    def print_tree(self):
        for pre, fill, node in RenderTree(self.root):
            print(f'{pre}{node}')

    def _find_best_moves_combination(self, perform_time=20):
        self._instantiate_root(state=deepcopy(self.game))
        if len(CombinationBuilder(self.root.state).possible_moves) == 0:
            return []

        start = time()
        while time() - start < perform_time:
            node = self._select(self.root)
            expanded_node = self._expand(node)

            if expanded_node is None:
                continue

            winner = self._simulate(expanded_node)
            self._propagate(expanded_node, winner)

        return self._get_best_moves_combination()

    def _select(self, node):
        """Return one of descendant nodes balancing exploration and exploitation."""
        if node.is_leaf or not self._is_fully_expanded(node):
            return node

        if node == self.root:
            children = node.children
        else:
            children = []
            for child in node.children:
                children += child.children

        new_node = max(children, key=lambda n: self._get_node_rate(n, node.simulations))
        return self._select(new_node)

    def _expand(self, node):
        """Expand from a given node by creating all possible children and randomly choosing one."""
        builder = CombinationBuilder(node.state)
        new_state, combination, chosen_card = builder.random_combination

        if len(combination) == 0 or self._find_node(new_state, node):
            return None

        if node == self.root:
            return self._add_node(state=new_state, action=combination, parent=node)

        try:
            card_choice_node = next(n for n in node.children if n.card_name == chosen_card.name)
        except StopIteration:
            card_choice_node = self._add_card_choice_node(parent=node, card_name=chosen_card.name)

        return self._add_node(state=new_state, action=combination, parent=card_choice_node)

    @staticmethod
    def _is_fully_expanded(node, tests_to_perform=3):
        """Node is fully expand if all possible moves were took from it."""
        found_equal_states = 0

        if not node.is_fully_expanded:

            for i in range(tests_to_perform):
                builder = CombinationBuilder(node.state)
                new_state = builder.random_combination[0]

                if findall(
                        node,
                        lambda n: n.__class__ == StateNode and n.state.state_hash == new_state.state_hash,
                        maxlevel=2
                ):
                    found_equal_states += 1

            if found_equal_states == tests_to_perform:
                node.is_fully_expanded = True

        return node.is_fully_expanded

    @staticmethod
    def _simulate(node):
        """Simulate random playout and return winner."""
        state = node.state
        game = deepcopy(state)
        player = FastRandomGreedyPlayer(game=game)
        opponent = FastRandomGreedyPlayer(game=game)

        while game.winner == Winner.NONE:
            if game.current_player == game.player:
                player.perform_moves()
            else:
                opponent.perform_moves()

        return game.winner

    @staticmethod
    def _propagate(node, winner):
        """Propagate result with given winner back to the root."""
        while node:
            if node.__class__ == StateNode:
                if winner == Winner.PLAYER and node.state.current_player == node.state.player:
                    node.wins += 1
                elif winner == Winner.OPPONENT and node.state.current_player == node.state.opponent:
                    node.wins += 1
                node.simulations += 1
            node = node.parent

    def _instantiate_root(self, state):
        self.root = self._find_node(state) or self._add_node(state)

    def _find_node(self, state, from_node=None):
        """Return node with a given state of game if such exists or None elsewhere."""
        if from_node is None:
            from_node = self.root

        return find(
            from_node,
            lambda node: hasattr(node, 'state') and node.state.state_hash == state.state_hash,
            maxlevel=2
        )

    @staticmethod
    def _find_nodes(state, from_node, max_level=2):
        return findall(from_node, lambda node: node.state.state_hash == state.state_hash, maxlevel=max_level)

    def _get_best_moves_combination(self):
        """Return best move in a tree."""
        if not self.root.children:
            return []

        return min(self.root.children, key=lambda n: n.wins / n.simulations if n.simulations != 0 else 0).action

    @staticmethod
    def _add_card_choice_node(parent, card_name):
        return CardChoiceNode(parent=parent, card_name=card_name)

    @staticmethod
    def _add_node(state, action=None, parent=None):
        return StateNode(parent=parent, state=state, action=action)

    @staticmethod
    def _get_node_rate(node, parent_node_visits, bias=sqrt(2)):
        """Return node rate based on UCT formula.

        First element is responsible for exploitation. Rates better nodes having more wins in all simulations.
        Second element is responsible for exploration. Rates better nodes having less simulations carried out."""
        return (node.wins / node.simulations) + bias * sqrt(log(parent_node_visits) / node.simulations)
