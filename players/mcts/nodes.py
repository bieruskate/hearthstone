from anytree import NodeMixin


class StateNode(NodeMixin):
    def __init__(self, parent, state, action):
        self.parent = parent
        self.wins = 0
        self.simulations = 0
        self.state = state
        self.state_hash = state.state_hash
        self.action = action
        self.is_fully_expanded = False

    def __str__(self):
        return f'{self.wins} / {self.simulations}'


class CardChoiceNode(NodeMixin):
    def __init__(self, parent, card_name):
        self.parent = parent
        self.card_name = card_name

    def __str__(self):
        return self.card_name
