from copy import deepcopy


class StateEvaluationMixin:
    def aggressive_key_func(self, game, combination, game_after=None):
        if not game_after:
            game_after = self._get_game_state_after(game, combination)

        cards_on_table_diff, minions_diff, opponent_diff = self._calculate_diffs(game, game_after)

        return opponent_diff * 100 + minions_diff * 10 + cards_on_table_diff

    def controlling_key_func(self, game, combination, game_after=None):
        if not game_after:
            game_after = self._get_game_state_after(game, combination)

        cards_on_table_diff, minions_diff, opponent_diff = self._calculate_diffs(game, game_after)

        return opponent_diff * 10 + minions_diff * 100 + cards_on_table_diff

    @staticmethod
    def _calculate_diffs(game, game_after):
        opponent_diff = abs(game_after.current_opponent.hp - game.current_opponent.hp)

        minions_diff = abs(game_after.current_opponent.deck.get_all_active_minions_hp() -
                           game.current_opponent.deck.get_all_active_minions_hp())

        cards_on_table_diff = (len(game_after.current_player.deck.cards_on_table) -
                               len(game.current_player.deck.cards_on_table))

        return cards_on_table_diff, minions_diff, opponent_diff

    def _get_game_state_after(self, game, combination):
        game_copy, combination_copy = self._copy_game_and_combination(game, combination)
        for move in combination_copy:
            game_copy.handle_move(move)
        return game_copy

    @staticmethod
    def _copy_game_and_combination(game, combination):
        game_copy = deepcopy(game)
        combination_copy = deepcopy(combination)

        for move in combination_copy:
            move.card = game_copy.current_player.deck.get_card_by_pk(move.card.pk)

            if hasattr(move, 'card_to_attack'):
                move.card_to_attack = game_copy.current_opponent.deck.get_card_by_pk(move.card_to_attack.pk)
            elif hasattr(move, 'opponent'):
                move.opponent = game_copy.current_opponent

        return game_copy, combination_copy
