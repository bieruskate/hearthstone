from time import time

from game.combinations_builder import CombinationBuilder
from game.game import Winner
from logger.logger import print_with_color, Color
from players.greedy.state_evaluation_mixin import StateEvaluationMixin


class GreedyPlayer:
    def __init__(self, game, debug=False):
        self.debug = debug
        self.game = game
        self.key_func = None
        self.combo_builder = CombinationBuilder(game)

    def perform_moves(self):
        moves = self._select_moves()

        for move in moves:
            if self.debug:
                print_with_color(move, Color.BLUE)

            self.game.handle_move(move)
            if self.game.winner != Winner.NONE:
                break

        if len(moves) == 0:
            print_with_color('Brak możliwych ruchów do wykonania', Color.WARNING)

        self.game.next_tour()

    def _select_moves(self, perform_time=3):
        best_combination = []
        best_grade = 0

        start = time()
        while time() - start < perform_time:
            builder = CombinationBuilder(self.game)
            game_after, combination, _ = builder.random_combination

            grade = self.key_func(self.game, combination, game_after)

            if grade > best_grade:
                best_grade = grade
                best_combination = combination

        return best_combination


class AggressiveGreedyPlayer(GreedyPlayer, StateEvaluationMixin):
    def __init__(self, game, debug=False):
        super().__init__(game, debug)
        self.key_func = self.aggressive_key_func


class ControllingGreedyPlayer(GreedyPlayer, StateEvaluationMixin):
    def __init__(self, game, debug=False):
        super().__init__(game, debug)
        self.key_func = self.controlling_key_func


class FastRandomGreedyPlayer(GreedyPlayer):
    def perform_moves(self):
        move = self.combo_builder.get_random_move()

        while move:
            self.game.handle_move(move)
            move = self.combo_builder.get_random_move()

        self.game.next_tour()
