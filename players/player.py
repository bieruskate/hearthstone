from deck.deck import Deck


class Player:

    def __init__(self, name, cards_on_start):
        self.name = name
        self.hp = 20
        self.deck = Deck()
        self.deck.get_random_cards(cards_on_start)
        self.__out_of_cards_multiplier = 1

    def __str__(self):
        name = f'{self.name} ({self.hp})'
        return name.upper()

    def start_tour(self, random_card=False):
        if not random_card:
            cards = self.deck.get_random_cards(1)
        else:
            cards = [self.deck.choose_card_based_on_draw_probabilities()]

        self._handle_out_of_cards_case(cards)
        self.deck.allow_attack_for_cards_on_table()

        return cards[0] if len(cards) > 0 else None

    def _handle_out_of_cards_case(self, cards):
        if len(cards) == 0:
            self.hp -= self.__out_of_cards_multiplier
            self.__out_of_cards_multiplier += 1
