import random
from copy import deepcopy

import numpy as np

from .card import Minion, SpecialMinion, Curse, CardState


class Deck:
    def __init__(self):
        cards = [
            Minion(1, 'Murlok grabieżca', cost=1, attack=2, hp=1),
            Minion(2, 'Bagienny raptor', 2, 3, 2),
            Minion(3, 'Wulkaniczny furiat', 3, 5, 1),
            Minion(4, 'Yeti z Zimnych Wichrów', 4, 4, 5),
            Minion(5, 'Ognisty ogar', 7, 9, 5),
            SpecialMinion(6, 'Gobliński ochroniarz', 5, 5, 4, 'Prowokacja'),
            SpecialMinion(7, 'Tygrys z Gierniodławów', 5, 5, 5, 'Ukrycie', is_hidden=True),
            Curse(8, 'Tajemna eksplozja', 2, 'Zadaj 1 pkt. obrażeń wszystkim wrogim stronnikom'),
            Curse(9, 'Kula ognia', 4, 'Zadaj 6 pkt. obrażeń'),
            Curse(10, 'Polimorfia', 4, 'Przemień stronnika w Owcę 1/1')
        ]
        cards_copy = deepcopy(cards)
        for card in cards_copy:
            card.pk += 10

        self.cards = [*cards, *cards_copy]
        np.random.shuffle(self.cards)

    @property
    def cards_in_deck(self):
        return self._get_cards_by_state(CardState.IN_DECK)

    @property
    def cards_on_hand(self):
        return self._get_cards_by_state(CardState.ON_HAND)

    @property
    def cards_on_table(self):
        return self._get_cards_by_state(CardState.ON_TABLE)

    @property
    def cards_able_to_attack(self):
        return [c for c in self.cards_on_table if c.can_attack]

    @property
    def cards_with_provocation_on_table(self):
        return [c for c in self.cards_on_table if type(c) == SpecialMinion and c.skill == 'Prowokacja']

    def has_provocation_on_table(self):
        return len(self.cards_with_provocation_on_table) > 0

    def get_random_cards(self, count=1):
        cards_in_deck = self.cards_in_deck

        if len(cards_in_deck) >= count:
            chosen_cards = []
            for _ in range(count):
                index, card = random.choice(list(enumerate(cards_in_deck)))
                chosen_cards.append(card)
                chosen_cards[-1].state = CardState.ON_HAND

                del cards_in_deck[index]

            return chosen_cards

        return []

    def decrease_life_for_all_cards_on_table(self, value):
        for index, card in enumerate(self.cards):
            if card.state == CardState.ON_TABLE:
                card.hp -= value
                if card.hp <= 0:
                    del self.cards[index]

    def allow_attack_for_cards_on_table(self):
        for card in self.cards_on_table:
            card.can_attack = True

    def remove_card(self, card_pk):
        for index, card in enumerate(self.cards):
            if card.pk == card_pk:
                del self.cards[index]
                break

    def get_card_by_pk(self, pk):
        return next(c for c in self.cards if c.pk == pk)

    def _get_cards_by_state(self, state):
        return [c for c in self.cards if c.state == state]

    def get_all_active_minions_hp(self):
        return sum([card.hp for card in self.cards_on_table])

    def choose_card_based_on_draw_probabilities(self):
        probabilities_ranges = self._get_card_draw_probabilities_dict()
        draft = np.random.uniform()

        for name, (left_range, right_range) in probabilities_ranges.items():
            if left_range <= draft < right_range:
                card = self._get_card_in_deck_by_name(name)
                card.state = CardState.ON_HAND
                return card

        return None

    def _get_card_in_deck_by_name(self, card_name):
        return next(c for c in self.cards_in_deck if c.name == card_name)

    def _get_card_draw_probabilities_dict(self):
        probabilities = {card.name: 0 for card in self.cards_in_deck}
        card_draw_probability = 1 / len(self.cards_in_deck)

        for card in self.cards_in_deck:
            probabilities[card.name] += card_draw_probability

        probabilities_sum = 0
        probabilities_ranges = {}
        for name, prob in probabilities.items():
            new_sum = probabilities_sum + prob
            probabilities_ranges[name] = (probabilities_sum, new_sum)

            probabilities_sum = new_sum

        return probabilities_ranges
