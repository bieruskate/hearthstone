from enum import Enum

from game.actions import CardAction


class CardState(Enum):
    ON_TABLE = 1
    ON_HAND = 2
    IN_DECK = 3


class BaseCard:
    def __init__(self, pk, name, cost):
        self.pk = pk
        self.name = name
        self.cost = cost
        self.state = CardState.IN_DECK

    def __str__(self):
        return f'({self.pk:{2}}) {self.name:{25}} M: {self.cost:{2}}'

    def put_on_table(self):
        self.state = CardState.ON_TABLE

    def get_possible_moves(self, **kwargs):
        return NotImplementedError('Possible moves not defined for that class')

    def get_id(self):
        return self.pk if self.pk <= 10 else self.pk - 10


class Minion(BaseCard):
    def __init__(self, pk, name, cost, attack, hp):
        super().__init__(pk, name, cost)
        self.attack = attack
        self.hp = hp
        self.can_attack = False

    def __str__(self):
        return super().__str__() + \
               f' A: {self.attack:{2}} HP: {self.hp:{2}}' \
               f' {"⚔" if self.can_attack else ""}'

    def attack_card(self, card):
        card.hp -= self.attack
        self.hp -= card.attack
        self.can_attack = False

    def attack_opponent(self, opponent):
        opponent.hp -= self.attack
        self.can_attack = False

    def get_possible_moves(self, mana):
        if self.state == CardState.ON_HAND and self.cost <= mana:
            return {CardAction.PLACE_ON_TABLE}
        if self.can_attack and self.state == CardState.ON_TABLE:
            return {CardAction.ATTACK_OTHER_CARD, CardAction.ATTACK_OPPONENT, CardAction.ATTACK_MINION_WITH_PROVOCATION}
        return {}

    def change_stats(self, attack, hp, name):
        self.attack = attack
        self.hp = hp
        self.name = name

    def decrease_hp(self, value):
        self.hp -= value


class SpecialMinion(Minion):
    def __init__(self, pk, name, cost, attack, hp, skill, is_hidden=False):
        super().__init__(pk, name, cost, attack, hp)
        self.skill = skill
        self.is_hidden = is_hidden

    def __str__(self):
        return super().__str__() + f' S: {self.skill}'

    def attack_card(self, card):
        super().attack_card(card)
        self.is_hidden = False


class Curse(BaseCard):
    def __init__(self, pk, name, cost, skill):
        super().__init__(pk, name, cost)
        self.skill = skill
        self.attack = 6

    def __str__(self):
        return super().__str__() + f'{" ":{14}} S: {self.skill}'

    def get_possible_moves(self, mana):
        if self.state == CardState.ON_HAND and self.cost <= mana:
            return {CardAction.PLAY_SPELL}

    @staticmethod
    def attack_opponent(opponent, damage=6):
        opponent.hp -= damage

    @staticmethod
    def attack_card(card, damage=6):
        card.hp -= damage
