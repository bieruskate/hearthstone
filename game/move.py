class Move:
    def __init__(self, card):
        self.card = card

    def __str__(self):
        return f'{self.__class__.__name__} : '


class PlaceOnTableMove(Move):
    def __init__(self, card):
        super().__init__(card)

    def __str__(self):
        return super().__str__() + f'({self.card.name})'


class AttackMinionMove(Move):
    def __init__(self, card, card_to_attack):
        super().__init__(card)
        self.card_to_attack = card_to_attack

    def __str__(self):
        return super().__str__() + f'({self.card.name}, {self.card_to_attack})'


class AttackOpponentMove(Move):
    def __init__(self, card, opponent):
        super().__init__(card)
        self.opponent = opponent

    def __str__(self):
        return super().__str__() + f'({self.card.name}, {self.opponent})'


class AttackEnemyMinionsMove(Move):
    def __init__(self, card, damage=1):
        super().__init__(card)
        self.damage = damage

    def __str__(self):
        return super().__str__() + f'({self.card.name}, {self.damage})'


class TransformMinionMove(Move):
    def __init__(self, card, card_to_attack):
        super().__init__(card)
        self.card_to_attack = card_to_attack

    def __str__(self):
        return super().__str__() + f'({self.card.name}, {self.card_to_attack})'
