from game.actions import CardAction, TargetAction
from game.game import Winner, MoveNotAllowedError
from players.greedy.greedy_player import AggressiveGreedyPlayer
from players.mcts.mcts import MCTS


class GameViewHandler:
    def __init__(self, game, view, against_bot=False, against_mcts=False):
        self.view = view
        self.game = game
        self.against_bot = against_bot
        self.against_mcts = against_mcts
        if self.against_bot:
            self.bot = AggressiveGreedyPlayer(game, debug=True)
        elif self.against_mcts:
            self.bot = MCTS(game)
        self.player = MCTS(game)

    def start_game(self):
        self.game.start()

        while self.game.winner == Winner.NONE:
            self.view.display(debug=False)
            if (self.against_bot or self.against_mcts) and self.game.current_player == self.game.opponent:
                self._handle_bot_move()
            else:
                # self.player.perform_moves()
                self._handle_human_move()

        self.view.display_winner()

        return self.game.winner

    def _handle_bot_move(self):
        self.bot.perform_moves()

    def _handle_human_move(self):
        self.view.display_actions()
        action = self.view.get_action()

        if self.game.is_move_card_action(action):
            action, card, kwargs = self._handle_move_action()

            try:
                self.game.handle_card_action(card, action, **kwargs)
            except MoveNotAllowedError as e:
                msg = str(e).upper()
                print('\n' + msg)
        else:
            self.game.next_tour()

    def _handle_move_action(self):
        card = self.view.get_card()

        self.view.display_card_actions(card)
        card_action = self.view.get_card_action()

        kwargs = {}
        if card_action in [CardAction.ATTACK_OTHER_CARD, CardAction.ATTACK_MINION_WITH_PROVOCATION]:
            kwargs['card_to_attack'] = self.view.get_card_to_attack()

        elif card_action == CardAction.PLAY_SPELL:
            self._handle_play_spell_action(card, kwargs)
        return card_action, card, kwargs

    def _handle_play_spell_action(self, card, kwargs):
        if card.name == 'Polimorfia':
            kwargs['card_to_attack'] = self.view.get_card_to_attack()

        elif card.name == 'Kula ognia':
            self.view.display_possible_attack_targets()
            attack_target = self.view.get_attack_target()

            if attack_target == TargetAction.ATTACK_MINION:
                kwargs['card_to_attack'] = self.view.get_card_to_attack()
