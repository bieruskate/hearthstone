from deck.card import CardState, Curse, Minion, SpecialMinion
from game.game import Tour, CardAction, Action, Winner
from game.actions import TargetAction
from game.combinations_builder import CombinationBuilder


class GameView:
    def __init__(self, game):
        self.game = game
        self.combo_builder = CombinationBuilder(game)

    def display(self, debug=False):
        opponent = self.game.opponent
        print(f'\n--> {opponent} <--\n') if self.game.whose_tour == Tour.OPPONENT else print(f'\n{opponent}\n')
        print('Cards on hand:')
        self.display_cards(self.game.opponent.deck.cards_on_hand)
        print('\nCards on table:')
        self.display_cards(self.game.opponent.deck.cards_on_table)
        print(f'\n========================== MANA: {self.game.mana}\n')
        print('Cards on table:')
        self.display_cards(self.game.player.deck.cards_on_table)
        print('\nCards on hand:')
        self.display_cards(self.game.player.deck.cards_on_hand)
        player = self.game.player
        print(f'\n--> {player} <--\n') if self.game.whose_tour == Tour.PLAYER else print(f'\n{player}\n')

        if debug:
            print('----------------------------')
            print('\n(debug) POSSIBLE MOVES:\n')
            self.display_possible_moves()
            print('\n----------------------------\n')

            print('\n(debug) POSSIBLE MOVES COMBINATIONS:\n')
            self.display_possible_moves_combinations()
            print('\n----------------------------\n')

    @staticmethod
    def display_cards(cards):
        for card in cards:
            print(card)

    def display_possible_moves(self):
        for move in self.combo_builder.possible_moves:
            print(move)

    def display_possible_moves_combinations(self):
        for combination in self.combo_builder.moves_combinations:
            for move in combination:
                print(move)
            print()

    @staticmethod
    def display_actions():
        print(f'({Action.MOVE_CARD.value}) Move card')
        print(f'({Action.FINISH_TOUR.value}) Finish tour')

    def display_card_actions(self, card):
        if card.state == CardState.ON_HAND:
            if type(card) in [Minion, SpecialMinion]:
                print(f'({CardAction.PLACE_ON_TABLE.value}) Place card on table')
            elif type(card) == Curse:
                print(f'({CardAction.PLAY_SPELL.value}) Play spell')

        elif card.state == CardState.ON_TABLE:
            if self.game.current_opponent.deck.has_provocation_on_table():
                print(f'({CardAction.ATTACK_MINION_WITH_PROVOCATION.value}) Attack minion with provocation')
            else:
                print(f'({CardAction.ATTACK_OTHER_CARD.value}) Attack other card')
                print(f'({CardAction.ATTACK_OPPONENT.value}) Attack opponent')

    @staticmethod
    def display_possible_attack_targets():
        print(f'({TargetAction.ATTACK_MINION.value}) Attack minion')
        print(f'({TargetAction.ATTACK_OPPONENT.value}) Attack opponent')

    def display_winner(self):
        if self.game.winner == Winner.PLAYER:
            print('Gratulacje! Wygrałeś grę.')
        elif self.game.winner == Winner.OPPONENT:
            print('Przeciwnik wygrał grę.')

    def get_card(self):
        print('Choose card:')
        card_pk = int(input())
        return self.game.current_player.deck.get_card_by_pk(card_pk)

    def get_card_to_attack(self):
        print('Choose card to attack:')
        card_pk = int(input())
        return self.game.current_opponent.deck.get_card_by_pk(card_pk)

    @staticmethod
    def get_attack_target():
        print('Choose attack target:')
        attack_target = int(input())
        return TargetAction(attack_target)

    @staticmethod
    def get_action():
        print('Choose action:')
        action_pk = int(input())
        return Action(action_pk)

    @staticmethod
    def get_card_action():
        print('Choose action:')
        action_pk = int(input())
        return CardAction(action_pk)
