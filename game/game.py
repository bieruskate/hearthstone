from enum import Enum

from game.actions import CardAction, Action
from game.move import AttackMinionMove, PlaceOnTableMove, AttackEnemyMinionsMove, TransformMinionMove, \
    AttackOpponentMove
from players.player import Player


class MoveNotAllowedError(Exception):
    pass


class Tour(Enum):
    PLAYER = 1
    OPPONENT = 2


class Winner(Enum):
    PLAYER = 1
    OPPONENT = 2
    NONE = 3


class Game:
    def __init__(self):
        self.player = Player(name='Player', cards_on_start=3)
        self.opponent = Player(name='Opponent', cards_on_start=4)
        self.whose_tour = Tour.PLAYER
        self.tour = 0
        self.mana = 1

    @property
    def winner(self):
        if self.player.hp <= 0:
            return Winner.OPPONENT
        elif self.opponent.hp <= 0:
            return Winner.PLAYER
        return Winner.NONE

    @property
    def current_player(self):
        return (
            self.player if self.whose_tour == Tour.PLAYER
            else self.opponent)

    @property
    def current_opponent(self):
        return (
            self.opponent if self.whose_tour == Tour.PLAYER
            else self.player)

    def start(self):
        self.player.start_tour()

    def next_tour(self, random_card=False):
        self.tour += 1
        self.mana = min(int(self.tour / 2) + 1, 10)
        self.whose_tour = (
            Tour.OPPONENT if self.whose_tour == Tour.PLAYER
            else Tour.PLAYER)

        chosen_card = self.current_player.start_tour(random_card)
        return chosen_card

    @staticmethod
    def is_move_card_action(action):
        return action == Action.MOVE_CARD

    def handle_card_action(self, card, action, card_to_attack=None):
        if action not in card.get_possible_moves(self.mana):
            raise MoveNotAllowedError('Nie możesz wykonać tego ruchu tą kartą!')

        if action == CardAction.PLACE_ON_TABLE:
            card.put_on_table()
            self.mana -= card.cost

        elif action == CardAction.ATTACK_OTHER_CARD:
            self._handle_cards_fight(card, card_to_attack)

        elif action == CardAction.ATTACK_MINION_WITH_PROVOCATION:
            if card_to_attack not in self.current_opponent.deck.cards_with_provocation_on_table:
                raise MoveNotAllowedError("Musisz zaatakować stronnika z prowokacją!")
            else:
                self._handle_cards_fight(card, card_to_attack)

        elif action == CardAction.ATTACK_OPPONENT:
            card.attack_opponent(self.current_opponent)

        elif action == CardAction.PLAY_SPELL:
            self._handle_spell_action(card, card_to_attack)

    def handle_move(self, move):
        move.card = self.current_player.deck.get_card_by_pk(move.card.pk)

        if type(move) == PlaceOnTableMove:
            move.card.put_on_table()
            self.mana -= move.card.cost

        elif type(move) == AttackMinionMove:
            move.card_to_attack = self.current_opponent.deck.get_card_by_pk(move.card_to_attack.pk)
            self._handle_cards_fight(move.card, move.card_to_attack)
            if move.card.name == 'Kula ognia':
                self.current_player.deck.remove_card(card_pk=move.card.pk)

        elif type(move) == AttackOpponentMove:
            move.card.attack_opponent(self.current_opponent)
            if move.card.name == 'Kula ognia':
                self.current_player.deck.remove_card(card_pk=move.card.pk)

        elif type(move) == TransformMinionMove:
            move.card_to_attack = self.current_opponent.deck.get_card_by_pk(move.card_to_attack.pk)
            self._handle_spell_action(move.card, move.card_to_attack)

        elif type(move) == AttackEnemyMinionsMove:
            self._handle_spell_action(move.card)

    def _handle_spell_action(self, card, card_to_attack=None):
        self.mana -= card.cost
        if card.name == 'Tajemna eksplozja':
            self.current_opponent.deck.decrease_life_for_all_cards_on_table(1)

        elif card.name == 'Polimorfia':
            card_to_attack.change_stats(attack=1, hp=1, name='Owca')

        elif card.name == 'Kula ognia':
            if card_to_attack is None:
                card.attack_opponent(self.current_opponent, 6)
            else:
                card_to_attack.decrease_hp(6)
                self._remove_card_with_hp_lower_or_equal_0(card_to_attack, self.current_opponent)
        self.current_player.deck.remove_card(card_pk=card.pk)

    def _handle_cards_fight(self, card, card_to_attack):
        card = self.current_player.deck.get_card_by_pk(card.pk)
        card_to_attack = self.current_opponent.deck.get_card_by_pk(card_to_attack.pk)

        if hasattr(card_to_attack, 'skill') and card_to_attack.skill == 'Ukrycie' and card_to_attack.is_hidden:
            raise MoveNotAllowedError('Nie możesz atakować karty w ukryciu!')

        card.attack_card(card_to_attack)

        self._remove_card_with_hp_lower_or_equal_0(card, self.current_player)
        self._remove_card_with_hp_lower_or_equal_0(card_to_attack, self.current_opponent)

    @staticmethod
    def _remove_card_with_hp_lower_or_equal_0(card, player):
        if hasattr(card, 'hp') and card.hp <= 0:
            player.deck.remove_card(card_pk=card.pk)

    @property
    def state_hash(self):
        def get_player_state_hash(player):
            sep = '_'
            state = ''

            cards_on_hand = [card.get_id() for card in player.deck.cards_on_hand]
            cards_on_hand.sort()
            cards_on_hand = [str(c) for c in cards_on_hand]
            state += f'H({sep.join(cards_on_hand)})'

            cards_on_table = [(card.get_id(), card.hp) for card in player.deck.cards_on_table]
            cards_on_table.sort()
            cards_on_table = [f'{c[0]}:{c[1]}' for c in cards_on_table]
            state += f'+T({sep.join(cards_on_table)})'

            state += f'+P({player.hp})'

            return state

        return get_player_state_hash(self.player) + '+' + get_player_state_hash(self.opponent)
