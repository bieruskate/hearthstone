from enum import Enum


class Action(Enum):
    MOVE_CARD = 1
    FINISH_TOUR = 2


class CardAction(Enum):
    PLACE_ON_TABLE = 1
    ATTACK_OTHER_CARD = 2
    ATTACK_OPPONENT = 3
    PLAY_SPELL = 4
    ATTACK_MINION_WITH_PROVOCATION = 5


class TargetAction(Enum):
    ATTACK_MINION = 1
    ATTACK_OPPONENT = 2
