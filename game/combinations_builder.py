import itertools
from copy import deepcopy

import numpy as np

from deck.card import CardState, Minion, SpecialMinion, Curse
from game.move import PlaceOnTableMove, AttackMinionMove, AttackOpponentMove, AttackEnemyMinionsMove, \
    TransformMinionMove


class CombinationBuilder:
    def __init__(self, game):
        self.game = game

    @property
    def random_combination(self):
        move = self.get_random_move()
        combination = []
        self.game = deepcopy(self.game)

        while move:
            combination.append(move)
            self.game.handle_move(move)
            move = self.get_random_move()

        chosen_card = self.game.next_tour(random_card=True)

        return self.game, combination, chosen_card

    def get_random_move(self):
        return np.random.choice(self.possible_moves) if len(self.possible_moves) > 0 else None

    @property
    def possible_moves(self):
        return self._get_possible_attack_moves() + self._get_possible_place_card_on_table_moves()

    @property
    def moves_combinations(self):
        attack_combinations = self._get_attack_moves_combinations()
        place_on_table_combinations = self._get_place_card_on_table_moves_combinations()

        combinations = [tuple(itertools.chain.from_iterable(prod)) for prod in
                        itertools.product(attack_combinations, place_on_table_combinations)]

        combinations += attack_combinations
        combinations += place_on_table_combinations

        combinations = list(filter(lambda comb: self._has_enough_mana_for_combination(comb), combinations))

        return combinations

    def _get_possible_attack_moves(self):
        moves = []
        attackable_minions = filter(
            lambda c: not (c.name == 'Tygrys z Gierniodławów' and c.is_hidden),
            self.game.current_opponent.deck.cards_on_table)

        for card in self.game.current_player.deck.cards_on_hand:
            if card.cost > self.game.mana:
                continue

            if type(card) == Curse:
                if card.name == 'Tajemna eksplozja':
                    if len(self.game.current_opponent.deck.cards_on_table) >= 1:
                        moves.append(AttackEnemyMinionsMove(card))
                elif card.name == 'Polimorfia':
                    moves += [TransformMinionMove(card, m) for m in attackable_minions]
                elif card.name == 'Kula ognia':
                    moves += [AttackMinionMove(card, m) for m in attackable_minions]
                    moves.append(AttackOpponentMove(card, self.game.current_opponent))

        minions_to_attack = (
                self.game.current_opponent.deck.cards_with_provocation_on_table or attackable_minions)
        combinations = list(itertools.product(self.game.current_player.deck.cards_able_to_attack, minions_to_attack))

        moves += [AttackMinionMove(c[0], card_to_attack=c[1]) for c in combinations]

        if not self.game.current_opponent.deck.has_provocation_on_table():
            moves += [
                AttackOpponentMove(card, opponent=self.game.current_opponent)
                for card in self.game.current_player.deck.cards_able_to_attack
            ]

        return moves

    def _get_possible_place_card_on_table_moves(self):
        moves = []

        for card in self.game.current_player.deck.cards_on_hand:
            if card.cost <= self.game.mana and (type(card) == Minion or type(card) == SpecialMinion):
                moves.append(PlaceOnTableMove(card=card))

        return moves

    def _get_attack_moves_combinations(self):
        attack_moves = self._get_possible_attack_moves()
        combinations = self._get_all_combinations_or_permutations(itertools.permutations, attack_moves)
        combinations = [c for c in combinations if len(set([move.card for move in c])) == len(c)]
        combinations = [c for c in combinations if not self._has_overkill(c)]
        return combinations

    def _get_place_card_on_table_moves_combinations(self):
        moves = self._get_possible_place_card_on_table_moves()
        return self._get_all_combinations_or_permutations(itertools.combinations, moves)

    def _get_all_combinations_or_permutations(self, func, moves):
        possible_combinations = []

        for i in range(len(moves)):
            for combination in func(moves, i + 1):
                if self._has_enough_mana_for_combination(combination):
                    possible_combinations.append(combination)

        return possible_combinations

    def _has_enough_mana_for_combination(self, combination):
        return sum([move.card.cost for move in combination if move.card.state == CardState.ON_HAND]) <= self.game.mana

    def _has_overkill(self, combination):
        card_states_after = {card.pk: card.hp for card in self.game.current_opponent.deck.cards_on_table}
        opponent_state_after = self.game.current_opponent.hp

        for move in combination:
            if opponent_state_after <= 0:
                return True

            elif move.__class__ == AttackOpponentMove:
                opponent_state_after -= move.card.attack

            elif move.__class__ == AttackMinionMove:
                if card_states_after[move.card_to_attack.pk] <= 0:
                    return True
                card_states_after[move.card_to_attack.pk] -= move.card.attack

            elif move.__class__ == TransformMinionMove:
                if card_states_after[move.card_to_attack.pk] <= 0:
                    return True
                card_states_after[move.card_to_attack.pk] = 1

            elif move.__class__ == AttackEnemyMinionsMove:
                for card in self.game.current_opponent.deck.cards_on_table:
                    if card_states_after[card.pk] <= 0:
                        return True
                    card_states_after[card.pk] -= move.damage

        return False
