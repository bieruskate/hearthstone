class Color:
    BLUE = '\033[94m'
    WARNING = '\033[93m'
    BOLD = '\033[1m'


def print_with_color(message, color):
    print(color, message, '\033[0m')
